import React, {Component} from 'react';

import CollectionPreview from '../../components/collection-preview/collection-preview';
import {ItemCollection} from "../../shared/model/item-collection";
import {SHOP_DATA} from "../../shared/model/shop.data";

type ShopPageProps = {
    collections: Array<ItemCollection>;
}

type ShopPageState = {
    collections: Array<ItemCollection>;
}

class ShopPage extends Component<ShopPageProps, ShopPageState> {
    constructor(props) {
        super(props);
        this.state = {
            collections: SHOP_DATA
        };
    }

    render() {
        return (
            <div className='shop-page'>
                {this.state.collections
                    .map(({id, ...otherCollectionProps}) => (
                        <CollectionPreview key={id} {...otherCollectionProps} />
                    ))}
            </div>
        );
    }
}

export default ShopPage;
