import React, {Component} from 'react';
import FormInput from "../../components/form/form.input";
import Button from "../../components/buttons/button.component";
import {signInWithGoogle} from "../../firebase/firebase.utils";

type SignInProps = {}
type SignInState = {
    email: string;
    password: string;
}

class SignIn extends Component<SignInProps, SignInState> {
    constructor(props) {
        super(props);
        this.state = {email: "", password: ""}
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({email: "", password: ""})
    }

    handleChange = (event) => {
        const {value, name} = event.target;
        this.setState({[name]: value} as Pick<SignInState, keyof SignInProps>)
    }

    render() {
        const {email, password} = this.state;
        return (
            <div className="sign-in">
                <h2>I already have an account</h2>
                <span>Sign in with your email and password</span>
                <form onSubmit={this.handleSubmit}>
                    <FormInput
                        label="Email"
                        name="email"
                        type="email"
                        value={email}
                        handleChange={this.handleChange}
                        required/>
                    <FormInput
                        label="password"
                        name="password"
                        type="password"
                        value={password}
                        handleChange={this.handleChange}
                        required/>
                    <div className="buttons">
                        <Button
                            isGoogleSignIn={false}
                            className="button"
                            type="submit"
                            value="Submit form">
                            Sign in
                        </Button>
                        <Button
                            isGoogleSignIn
                            className="button"
                            onClick={signInWithGoogle}
                            type="submit"
                            value="Submit form">
                            Sign in with Google
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

export default SignIn;