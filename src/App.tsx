import './App.scss';
import React from 'react';
import Header from "./components/header/header.component";
import Routes from "./routes";
import {auth} from "./firebase/firebase.utils";
import {Observer, Unsubscribe, User} from "firebase";

type AppProps = {
    user: Observer<any> | User | null
}
type AppState = {
    currentUser: Observer<any> | User | null;
}

class App extends React.Component<AppProps, AppState> {
    unsubscribeFromAuth: Unsubscribe | null = null;

    constructor(props) {
        super(props);
        this.state = {currentUser: null}
    }

    componentDidMount(): void {
        this.unsubscribeFromAuth = auth.onAuthStateChanged(user =>
            this.setState({currentUser: user}))
    }

    componentWillUnmount(): void {
        // @ts-ignore
        this.unsubscribeFromAuth();
    }

    render() {
        return (
            <div>
                <Header user={this.state.currentUser}/>
                <Routes/>
            </div>
        );
    }
}

export default App;
