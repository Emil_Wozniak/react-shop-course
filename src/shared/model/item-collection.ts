import {Item} from "./item";

export interface ItemCollection {
    id: number;
    title: string;
    routeName: string;
    items: Array<Item>;
}

export const defaultItemCollection = (): ItemCollection => ({
    id: 0,
    title: "",
    routeName: "",
    items: []
})
