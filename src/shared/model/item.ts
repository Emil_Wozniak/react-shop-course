export interface Item {
    id: number;
    name: string;
    imageUrl: string;
    price: number;
}

export const defaultItem = (): Item => ({
    id: 0,
    name: "",
    imageUrl: "",
    price: 0
})