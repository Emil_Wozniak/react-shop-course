import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

/**
 * Your web app's Firebase configuration
 */
const config = {
    apiKey: "AIzaSyDjxuVJswn08BsEWXyFNv6vUoHOSHW2aCA",
    authDomain: "shop-f0027.firebaseapp.com",
    databaseURL: "https://shop-f0027.firebaseio.com",
    projectId: "shop-f0027",
    storageBucket: "shop-f0027.appspot.com",
    messagingSenderId: "1091479544971",
    appId: "1:1091479544971:web:91d589aa764dc08331e08e",
    measurementId: "G-0482E5XHX7"
};
/**
 * Initialize Firebase
 */
firebase.initializeApp(config);
// firebase.analytics();


/**
 * AUTH
 */
export const auth = firebase.auth();
export const firestore = firebase.firestore();
const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({prompt: 'select_account'});
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;