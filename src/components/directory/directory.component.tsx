import React from 'react';

import MenuItem from '../menu-item/menu-item.component';

import './directory.styles.scss';
import {DIR_DATA} from "./directory.data";

type DirectoryProps = {}
type DirectoryState = {
    sections: Array<any>
}

class Directory extends React.Component<DirectoryProps, DirectoryState> {
    constructor(props) {
        super(props);
        this.state = DIR_DATA
    }

    render() {
        return (
            <div className='directory-menu'>
                {this.state.sections
                    .map(({id, ...otherSectionProps}) => (
                        <MenuItem key={id} {...otherSectionProps} />
                    ))}
            </div>
        );
    }
}

export default Directory;
