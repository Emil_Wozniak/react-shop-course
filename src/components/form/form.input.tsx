import "./form.styles.scss";
import React from 'react';
import {Form} from "./form";

const FormInput = ({handleChange, label, ...otherProps}: Form) => (
    <div className="group">
        <input
            className="form-input"
            onChange={(event) => handleChange(event)}
            {...otherProps}/>
        {label
            ? <label className={`${otherProps.value.length ? 'shrink' : ''} form-input-label`}>
                {label}
            </label>
            : <div/>}
    </div>
);

export default FormInput;