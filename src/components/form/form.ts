export interface Form {
    handleChange: Function;
    label: string;
    value: any;
    name:string;
    type: any;
    required: boolean;
}