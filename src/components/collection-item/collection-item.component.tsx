import './collection-item.styles.scss';
import React from 'react';
import {Item} from "../../shared/model/item";

const CollectionItem = ({ name, price, imageUrl}: Item) => (
    <div className='collection-item'>
        <div
            className='image'
            style={{backgroundImage: `url(${imageUrl})`}}
        />
        <div className='collection-footer'>
            <span className='name'>{name}</span>
            <span className='price'>{price}</span>
        </div>
    </div>
);

export default CollectionItem;
