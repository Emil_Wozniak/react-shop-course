import "./header.component.scss"
import React from 'react';
import {Observer, User} from "firebase";
import {ReactComponent as Logo} from "../../assets/slides.svg";
import {Link} from "react-router-dom";
import {auth} from "../../firebase/firebase.utils";

type HeaderProps = {
    user:  Observer<any> | User | null;
}

const Header = ({user}: HeaderProps) => (
    <div className="header">
        <Link to="/">
            <Logo className="logo-container"/>
        </Link>
        <div className="options">
            <Link className="option" to="/shop">
                SHOP
            </Link>
            <Link className="option" to="/contact">
                CONTACT
            </Link>
            {user
                ? <div className="option" onClick={() => auth.signOut()}>
                    SIGN OUT
            </div>
                : <Link to="/signin" >
                SING IN
                </Link>}
        </div>
    </div>
);

export default Header;