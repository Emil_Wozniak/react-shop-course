import "./button.styles.scss";
import React from 'react';

type ButtonProps = {
    isGoogleSignIn?: boolean;
    className?: string;
    onClick?: Function
    children?: any;
    type: any;
    value: string;
}

const Button = ({onClick, isGoogleSignIn, className, children, ...otherProps}: ButtonProps) => (
    <button
        className={`${isGoogleSignIn ? "google-sign-in" : ""} custom-button`}
        onClick={(event) => onClick ? onClick(event) : null}
        {...otherProps}>
        {children}
    </button>
);

export default Button;