import './collection-preview.styles.scss';
import React from 'react';
import CollectionItem from '../collection-item/collection-item.component';
import {Item} from "../../shared/model/item";

interface ICollectionPreview {
    title: string;
    items: Array<Item>
}

const CollectionPreview = ({title, items}: ICollectionPreview) => (
    <div className='collection-preview'>
        <h1 className='title'>
            {title.toUpperCase()}
        </h1>
        <div className='preview'>
            {items
                .filter((item, idx) => idx < 4)
                .map(({id, ...itemProps}) => (
                    <CollectionItem key={`items-${id}`} id={id} {...itemProps} />
                ))}
        </div>
    </div>
);

export default CollectionPreview;
