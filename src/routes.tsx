import React from 'react';
import {Route, Switch} from 'react-router-dom';
import HomePage from "./pages/home/home.component";
import ShopPage from "./pages/shop/shop.component";
import Account from "./pages/user/account";

const Routes = () => (
    <div style={{paddingTop: 90}}>
        <Switch>
            <Route exact path='/' component={HomePage}/>
            <Route path='/shop' component={ShopPage}/>
            <Route path='/signin' component={Account}/>
        </Switch>
    </div>
);

export default Routes;